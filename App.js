import React, {useState, useEffect} from 'react';
import {View,Text,FlatList,TouchableOpacity,StatusBar,Image,ScrollView, ImageBackground,} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';


const Home = () => {
  const [kategori, setKategori] = useState([
    {
        nama: 'Terbaru ',
      },
    {
      nama: 'Sport/Olahraga',
    },
    {
      nama: 'Entertaiment',
    },
    {
      nama: 'Makanan Khas',
    },
    {
      nama: 'Pariwisata',
    },
    {
      nama: 'Kabupaten',
    },
    {
      nama: 'Madura',
    },
    {
      nama: 'Tradisi',
    },
  ]);

  const [kategoriSeleksi, setKategoriSeleksi] = useState({name:'Terbaru'});

  const [dataTrending, setDataTrending] = useState([
    {
      namaResep: 'Potret VollyBall Di Sore Hari  ',
      author: 'Oleh : User6537',
      image: require('./src/images/10.jpg'),
    },
    {
      namaResep: 'Konser Musik',
      author: 'Oleh : Widjoyo',
      image: require('./src/images/11.jpg'),
    },
    {
      namaResep: 'Bebek Sinjay Bangkalan',
      author: 'Oleh : CEO Rizki',
      image: require('./src/images/7.jpg'),
    },
    {
      namaResep: 'Kerapan Sapi "Tradisi Madura"',
      author: 'Oleh : Fathoni',
      image: require('./src/images/14.jpg'),
    },
    {
      namaResep: 'Sate Madura',
      author: 'Oleh : Rekan Syaiq',
      image: require('./src/images/5.jpg'),
    },
    {
      namaResep: 'Pantai LON MALANG',
      author: 'Oleh : Rekan Syaiq',
      image: require('./src/images/12.jpg'),
    },
    {
      namaResep: 'Kabupaten Peraih Rekor Muri',
      author: 'Oleh : Taufik',
      image: require('./src/images/13.jpg'),
    },
  ]);

  const [dataVideo, setDataVideo] = useState([
    {
      namaResep: 'Alun Alun Bangkalan',
      author: 'sinal abidin',
      image: require('./src/images/26.jpg'),
      length: '05:08',
    },
    {
      namaResep: 'Wisata Religi Syaikh Kholil ',
      author: 'Nanda',
      image: require('./src/images/25.jpg'),
      length: '06:59',
    },
    {
      namaResep: 'Alun-Alun Sampang ',
      author: 'Fernando',
      image: require('./src/images/23.jpg'),
      length: '04:13',
    },
    {
      namaResep: 'Explore Pantai Camplong',
      author: 'Fathoni',
      image: require('./src/images/24.jpg'),
      length: '05:40',
    },
  ]);

  const [dataTerbaru, setDataTerbaru] = useState([
    {
      namaResep: 'Potret Jembatan Suramadu',
      author: 'Adi Hidayat',
      image: require('./src/images/16.jpg'),
      length: '03:35',
    },
    {
      namaResep: 'Potret Stadion Ratu Pamelingan ',
      author: 'Afiful',
      image: require('./src/images/18.jpg'),
      length: '05:00',
    },
    {
      namaResep: 'Explore Alam Di Madura',
      author: 'Faizah',
      image: require('./src/images/17.jpg'),
      length: '07:00',
    },
    {
      namaResep: 'Awal Mula Pulau Madura',
      author: 'Made',
      image: require('./src/images/15.jpg'),
      length: '18:00',
    },
  ]);
  const [dataMadura, setDataMadura] = useState([
    {
      namaResep: 'Sparing Di Stadion Sumenep',
      author: 'Yudoyono',
      image: require('./src/images/20.jpg'),
      length: '14:05',
    },
    {
      namaResep: 'Alun Alun Sumenep ',
      author: 'Mahrus',
      image: require('./src/images/22.jpg'),
      length: '05:00',
    },
    {
      namaResep: 'Stadion Bangkalan',
      author: 'Andre',
      image: require('./src/images/19.jpg'),
      length: '08:00',
    },
    {
      namaResep: 'Masjid Agung Sumenep',
      author: 'Rudi Ahmad',
      image: require('./src/images/21.jpg'),
      length: '09:55',
    },
  ]);

  return (
    <View style={{flex: 1, backgroundColor: '#f5f5f5'}}>
      <ScrollView>
        <StatusBar backgroundColor="#f5f5f5" barStyle="dark-content" />
        <View style={{marginHorizontal: 20, marginBottom: 20, marginTop: 20}}>
          <Text style={{fontSize: 28, fontWeight: 'bold', color: '#212121', textAlign:'center'}}>
            Info.<Text style={{color: '#f20511'}}>Madura</Text>
          </Text>
        </View>
        <View>
          <FlatList
            data={kategori}
            horizontal
            showsHorizontalScrollIndicator={false}
            style={{marginLeft: 10}}
            renderItem={({item}) => (
              <TouchableOpacity
                style={{
                  marginRight: 5,
                  backgroundColor:
                    kategoriSeleksi.nama == item.nama ? '#4169e1' : '#fff',
                  elevation: 3,
                  paddingHorizontal: 15,
                  paddingVertical: 8,
                  marginBottom: 10,
                  borderRadius: 15,
                  marginLeft: 5,
                }}
                onPress={() => setKategoriSeleksi(item)}>
                <Text
                  style={{
                    color:
                      kategoriSeleksi.nama == item.nama ? '#fff' : '#212121',
                  }}>
                  {item.nama}
                </Text>
              </TouchableOpacity>
            )}
          />
        </View>
        <View
          style={{
            marginHorizontal: 20,
            marginBottom: 10,
            marginTop: 20,
            flexDirection: 'row',
          }}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{fontSize: 24, fontWeight: 'bold', color: '#212121'}}>
              Trending
            </Text>
          </View>

          <TouchableOpacity
            style={{
              justifyContent: 'flex-end',
              alignItems: 'center',
              flex: 1,
              flexDirection: 'row',
              marginTop: 10,
            }}>
            <Text style={{fontSize: 14}}>Lihat Semua</Text>
            <Icon name="chevron-forward" size={20} color="#bdbdbd" />
          </TouchableOpacity>
        </View>

        <View>
          <FlatList
            data={dataTrending}
            horizontal
            showsHorizontalScrollIndicator={false}
            style={{marginLeft: 10}}
            renderItem={({item}) => (
              <TouchableOpacity
                style={{
                  marginRight: 5,
                  backgroundColor: '#fff',
                  elevation: 3,
                  paddingHorizontal: 15,
                  paddingVertical: 8,
                  marginBottom: 10,
                  borderRadius: 15,
                  marginLeft: 5,
                }}>
                <Image
                  source={item.image}
                  style={{
                    width: 200,
                    height: 150,
                    marginTop: 10,
                    marginBottom: 10,
                    borderRadius: 3,
                  }}
                  resizeMode={'stretch'}
                />
                <Text style={{ color: '#212121',fontSize: 18, fontWeight: 'bold',}}>{item.namaResep} </Text>
                <Text>{item.author}</Text>
              </TouchableOpacity>
            )} />
        </View>

            <View style={{ marginHorizontal: 20,marginBottom: 10,marginTop: 10,}}>
             
                <Text style={{fontSize: 24 , fontWeight: 'bold', color: '#212121', textAlign:'center'}}>
                -------------- V I D I O --------------</Text>
            </View>

        <ScrollView>
          <FlatList
            horizontal
            data={dataVideo}
            showsHorizontalScrollIndicator={false}
            style={{marginLeft: 10}} renderItem={({item}) => (
              <TouchableOpacity
                style={{ marginRight: 5,backgroundColor: '#fff',elevation: 3,
                  paddingHorizontal: 15,paddingVertical: 8,marginBottom: 10,
                  borderRadius: 15, marginLeft: 5,}}>
                <ImageBackground
                  source={item.image}
                  style={{ width: 200,height: 150,marginTop: 10,
                    marginBottom: 10,borderRadius: 3,}} resizeMode={'stretch'}>
                  <View style={{flex: 1}}>
                    <View style={{flex: 1}}></View>
                    <View
                  style={{ flexDirection: 'row', }}>
                      <View
                        style={{
                          justifyContent: 'center',
                          alignItems: 'center',
                          backgroundColor: 'rgba(0,0,0,0.7)',
                          paddingTop: 2,
                          paddingBottom: 2,
                        }}>
                        <Icon
                          style={{marginLeft: 5}}
                          name="play-circle"
                          size={15}
                          color="#bdbdbd"
                        />
                      </View>
                      <View
                        style={{
                          justifyContent: 'center',
                          alignItems: 'center',
                          backgroundColor: 'rgba(0,0,0,0.7)',
                          paddingRight: 10,
                          paddingTop: 2,
                          paddingBottom: 2,
                          paddingLeft: 4,
                        }}>
                        <Text style={{color: '#FFFFFF'}}>{item.length}</Text>
                      </View>
                    </View>
                  </View>
                </ImageBackground>
                <Text
                  style={{
                    color: '#212121',
                    fontSize: 18,
                    fontWeight: 'bold',
                  }}>
                  {item.namaResep}
                </Text>
                <Text>{item.author}</Text>
              </TouchableOpacity> )}/>
        </ScrollView>
        <ScrollView>
          <FlatList
            horizontal
            data={dataTerbaru}
            showsHorizontalScrollIndicator={false}
            style={{marginLeft: 10}} renderItem={({item}) => (
              <TouchableOpacity
                style={{ marginRight: 5,backgroundColor: '#fff',elevation: 3,
                  paddingHorizontal: 15,paddingVertical: 8,marginBottom: 10,
                  borderRadius: 15, marginLeft: 5,}}>
                <ImageBackground
                  source={item.image}
                  style={{ width: 200,height: 150,marginTop: 10,
                    marginBottom: 10,borderRadius: 3,}} resizeMode={'stretch'}>
                  <View style={{flex: 1}}>
                    <View style={{flex: 1}}></View>
                    <View
                  style={{ flexDirection: 'row', }}>
                      <View
                        style={{
                          justifyContent: 'center',
                          alignItems: 'center',
                          backgroundColor: 'rgba(0,0,0,0.7)',
                          paddingTop: 2,
                          paddingBottom: 2,
                        }}>
                        <Icon
                          style={{marginLeft: 5}}
                          name="play-circle"
                          size={15}
                          color="#bdbdbd"
                        />
                      </View>
                      <View
                        style={{
                          justifyContent: 'center',
                          alignItems: 'center',
                          backgroundColor: 'rgba(0,0,0,0.7)',
                          paddingRight: 10,
                          paddingTop: 2,
                          paddingBottom: 2,
                          paddingLeft: 4,
                        }}>
                        <Text style={{color: '#FFFFFF'}}>{item.length}</Text>
                      </View>
                    </View>
                  </View>
                </ImageBackground>
                <Text
                  style={{
                    color: '#212121',
                    fontSize: 18,
                    fontWeight: 'bold',
                  }}>
                  {item.namaResep}
                </Text>
                <Text>{item.author}</Text>
              </TouchableOpacity> )}/>
        </ScrollView>
        <ScrollView>
          <FlatList
            horizontal
            data={dataMadura}
            showsHorizontalScrollIndicator={false}
            style={{marginLeft: 10}} renderItem={({item}) => (
              <TouchableOpacity
                style={{ marginRight: 5,backgroundColor: '#fff',elevation: 3,
                  paddingHorizontal: 15,paddingVertical: 8,marginBottom: 10,
                  borderRadius: 15, marginLeft: 5,}}>
                <ImageBackground
                  source={item.image}
                  style={{ width: 200,height: 150,marginTop: 10,
                    marginBottom: 10,borderRadius: 3,}} resizeMode={'stretch'}>
                  <View style={{flex: 1}}>
                    <View style={{flex: 1}}></View>
                    <View
                  style={{ flexDirection: 'row', }}>
                      <View
                        style={{
                          justifyContent: 'center',
                          alignItems: 'center',
                          backgroundColor: 'rgba(0,0,0,0.7)',
                          paddingTop: 2,
                          paddingBottom: 2,
                        }}>
                        <Icon
                          style={{marginLeft: 5}}
                          name="play-circle"
                          size={15}
                          color="#bdbdbd"
                        />
                      </View>
                      <View
                        style={{
                          justifyContent: 'center',
                          alignItems: 'center',
                          backgroundColor: 'rgba(0,0,0,0.7)',
                          paddingRight: 10,
                          paddingTop: 2,
                          paddingBottom: 2,
                          paddingLeft: 4,
                        }}>
                        <Text style={{color: '#FFFFFF'}}>{item.length}</Text>
                      </View>
                    </View>
                  </View>
                </ImageBackground>
                <Text
                  style={{
                    color: '#212121',
                    fontSize: 18,
                    fontWeight: 'bold',
                  }}>
                  {item.namaResep}
                </Text>
                <Text>{item.author}</Text>
              </TouchableOpacity> )}/>
        </ScrollView>
        
      </ScrollView>
    </View>
  );
};

export default Home ;
